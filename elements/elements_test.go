package elements

import (
	"net/http"

	"github.com/getkin/kin-openapi/openapi3"
)

func ExampleNewHandler() {
	rawSpec := `
openapi: 3.1.0
info:
  version: 1.0.0
  title: Example OpenAPI spec
paths: {}
`

	loader := openapi3.NewLoader()
	doc, err := loader.LoadFromData([]byte(rawSpec))

	handler, err := NewHandler(doc, err)
	if err != nil {
		panic(err)
	}

	http.Handle("/docs", handler)

	// then we'd usually strart the server, i.e:
	// log.Fatal(http.ListenAndServe(":8080", nil))

	// Output:
}
