package elements

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"

	// required to embed the raw template
	_ "embed"

	"github.com/getkin/kin-openapi/openapi3"
)

//go:embed elements.tmpl
var templateRaw string

type templateData struct {
	SpecString string
}

// NewHandler creates a new http.HandlerFunc that produces an HTML-producing
// route, which uses Stoplight Elements' OpenAPI viewer for a given OpenAPI
// specification. Returns an error if the OpenAPI was invalid, or any errors
// are returned when trying to prepare the handler
func NewHandler(doc *openapi3.T, err error) (http.HandlerFunc, error) {
	if err != nil {
		return nil, fmt.Errorf("failed to construct an http.HandlerFunc, as the incoming OpenAPI provided was invalid: %v", err)
	}

	raw, err := doc.MarshalJSON()
	if err != nil {
		return nil, fmt.Errorf("failed to construct an http.HandlerFunc, as we could not marshal the OpenAPI to JSON: %v", err)
	}

	data := templateData{
		SpecString: string(raw),
	}

	tmpl, err := template.New("elements.tmpl").Parse(templateRaw)
	if err != nil {
		return nil, fmt.Errorf("failed to construct an http.HandlerFunc, as the template could not be parsed: %v", err)
	}
	var b bytes.Buffer

	err = tmpl.Execute(&b, data)
	if err != nil {
		return nil, fmt.Errorf("failed to construct an http.HandlerFunc, as the template could not be rendered: %v", err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "text/html; charset=utf-8")
		w.Write(b.Bytes())
	}, nil
}
